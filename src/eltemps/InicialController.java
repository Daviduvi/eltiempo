package eltemps;

import eltemps.domain.City;
import eltemps.domain.Weather;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

import java.util.ArrayList;
import java.util.List;

public class InicialController {

    private WeatherService weatherService = new WeatherService();
    private ListView<String> ciutats = new ListView<>();

    @FXML
    private Label lbInfo;

    @FXML
    private ListView lvView;

    public void initialize(){
        lvView.getItems().addAll("Barcelona", "Madrid", "Galicia", "Arenys de Mar", "Montcada i Reixac", "Ottawa");
        lvView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    Weather current = weatherService.getCurrentWeather(newValue.toString());
                    if (current != null) {
                        lbInfo.setText("Temp: " + current.getTemp() +
                                "\nFeels like: " + current.getFeelsLike() +
                                "\nMin: " + current.getMin() +
                                "\nMax: " + current.getMax() +
                                "\nPressure: " + current.getPressure() +
                                "\nHumidity: " + current.getHumidity());
                    }
                }
        );
    }

    public void testJSON(ActionEvent actionEvent) {

    }
}
